import { Injectable } from '@angular/core';
import axios from 'axios';
import { PDFDocument, rgb, StandardFonts, PDFPage, PDFName, PDFNumber, PDFHexString } from 'pdf-lib';
@Injectable({
  providedIn: 'root'
})
export class PdfService {

  constructor() { }
  async addWatermark(pdfBytes: ArrayBuffer, watermarkText: string): Promise<ArrayBuffer> {
    const pdfDoc = await PDFDocument.load(pdfBytes);
    const pages = pdfDoc.getPages();

    for (let i = 0; i < pages.length; i++) {
      const page = pages[i];
      const { width, height } = page.getSize();
      const font = await pdfDoc.embedFont(StandardFonts.Helvetica);

      page.drawText(watermarkText, {
        x: width / 2 - (watermarkText.length * 10) / 2,
        y: height / 2,
        size: 48,
        font,
        color: rgb(0.7, 0.7, 0.7),
      });
    }

    return await pdfDoc.save();
  }

  async addBookmarks(pdfBytes: ArrayBuffer, bookmarks: { title: string, pageNumber: number }[]): Promise<ArrayBuffer> {
    const pdfDoc = await PDFDocument.load(pdfBytes);
    const outlinesDict = pdfDoc.context.obj({});
    pdfDoc.catalog.set(PDFName.of('Outlines'), outlinesDict);

    const outlineItems = bookmarks.map((bookmark, index) => {
      const page = pdfDoc.getPage(bookmark.pageNumber - 1);
      const pageRef = page.ref;
      return pdfDoc.context.obj({
        Title: PDFHexString.fromText(bookmark.title),
        Dest: pdfDoc.context.obj([pageRef, PDFName.of('XYZ'), 0, page.getHeight(), 0])
      });
    });

    outlinesDict.set(PDFName.of('First'), outlineItems[0]);
    outlinesDict.set(PDFName.of('Last'), outlineItems[outlineItems.length - 1]);
    outlinesDict.set(PDFName.of('Count'), PDFNumber.of(bookmarks.length));

    outlineItems.forEach((item, index) => {
      if (index > 0) {
        item.set(PDFName.of('Prev'), outlineItems[index - 1]);
        outlineItems[index - 1].set(PDFName.of('Next'), item);
      }
    });

    return await pdfDoc.save();
  }

}
