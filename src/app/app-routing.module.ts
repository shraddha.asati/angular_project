import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RotatePageComponent } from './rotate-page/rotate-page.component';

const routes: Routes = [
  {path: '', component:RotatePageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
