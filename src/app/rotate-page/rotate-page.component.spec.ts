import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RotatePageComponent } from './rotate-page.component';

describe('RotatePageComponent', () => {
  let component: RotatePageComponent;
  let fixture: ComponentFixture<RotatePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RotatePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RotatePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
