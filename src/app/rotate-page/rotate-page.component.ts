import { Component, OnInit } from '@angular/core';
import { PdfService } from '../pdf.service';
import {PDFDocument, rgb, StandardFonts, PDFPage} from 'pdf-lib';



@Component({
  selector: 'app-rotate-page',
  templateUrl: './rotate-page.component.html',
  styleUrls: ['./rotate-page.component.scss']
})
export class RotatePageComponent implements OnInit {
  rotationDegree = 0;
  isOverlayVisible = false;
  isContentHidden = false;
  src: string = 'assets/ASP-Shraddha Asati.pdf';
  page: number = 1;
  totalPages: number = 0;
  isLoaded: boolean = false;
  pdfDoc: PDFDocument | undefined;
  selectedPages: number[] = [];
  highlightColor = rgb(1, 0, 0);
  comments: { page: number, x: number, y: number, text: string }[] = [];
  selectedFiles: File[] = [];
  file: File | undefined;
  watermarkText: string = '';
  bookmarks: { title: string, pageNumber: number }[] = [];
  bookmarkTitle: string = '';
  bookmarkPageNumber: number = 1;
  constructor(private watermarkService:PdfService) { }

  async ngOnInit(): Promise<void> {
    this.loadPdf();
   
  }

  //Ability to highlight some section and add comments
  async highlightSelection(startX: number, startY: number, endX: number, endY: number) {
    if (this.pdfDoc) {
      const pages = this.pdfDoc.getPages();
      const page = pages[this.page - 1];
      const { width, height } = page.getSize();

      page.drawRectangle({
        x: startX,
        y: height - startY,
        width: endX - startX,
        height: startY - endY,
        color: this.highlightColor,
        opacity: 0.5,
        borderWidth: 1,
      });

      await this.updatePdfViewer();
    }
  }

  addComment(text: string, x: number, y: number) {
    this.comments.push({ page: this.page, x, y, text });
  }

  removeComment(index: number) {
    this.comments.splice(index, 1);
  }

//Ability to import pdf and Ability to select some pages and share it as separate new document
  async loadPdf() {
    const pdfBytes = await fetch(this.src).then(res => res.arrayBuffer());
    this.pdfDoc = await PDFDocument.load(pdfBytes);
    this.totalPages = this.pdfDoc.getPageCount();
    this.isLoaded = true;
  }

  async updatePdfViewer() {
    if (this.pdfDoc) {
      const pdfBytes = await this.pdfDoc.save();
      const newBlob = new Blob([pdfBytes], { type: 'application/pdf' });
      this.src = URL.createObjectURL(newBlob);
      setTimeout(() => this.isLoaded = true, 0);
    }
  }

  async addText(text: string, x: number, y: number) {
    if (this.pdfDoc) {
      const pages = this.pdfDoc.getPages();
      const page = pages[this.page - 1];
      const { width, height } = page.getSize();
      const helveticaFont = await this.pdfDoc.embedFont(StandardFonts.Helvetica);

      page.drawText(text, {
        x: x,
        y: height - y,
        size: 24,
        font: helveticaFont,
        color: rgb(0, 0, 0),
      });

      this.savePdf();
    }
  }
  async savePdf() {
    if (this.pdfDoc) {
      const pdfBytes = await this.pdfDoc.save();
      const blob = new Blob([pdfBytes], { type: 'application/pdf' });
      const url = URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = 'edited.pdf';
      a.click();
    }
  }

  afterLoadComplete(pdfData: any) {
    this.totalPages = pdfData.numPages;
    this.isLoaded = true;
  }

  nextPage() {
    this.page++;
  }

  prevPage() {
    this.page--;
  }
 
// Ability to overlay or hide specific portion in the page
  toggleOverlay() {
    this.isOverlayVisible = !this.isOverlayVisible;
  }

  toggleHide() {
    this.isContentHidden = !this.isContentHidden;
  }

 

  //Using JavaScript
  // toggleHide() {
  //   this.isContentHidden = !this.isContentHidden;
  //   const contentElement = document.getElementById('overlayed-content');
  //   if (contentElement) {
  //     if (this.isContentHidden) {
  //       contentElement.classList.add('hidden-content');
  //     } else {
  //       contentElement.classList.remove('hidden-content');
  //     }
  //   }
  // }

//Ability to rotate page(clockwise, counter clockwise, 180 degree)
  rotateClockwise() {
    this.rotationDegree += 90;
  }

  rotateCounterClockwise() {
    this.rotationDegree -= 90;
  }

  rotate180() {
    this.rotationDegree += 180;
  }

  togglePageSelection(pageNumber: number) {
    const index = this.selectedPages.indexOf(pageNumber);
    if (index === -1) {
      this.selectedPages.push(pageNumber);
    } else {
      this.selectedPages.splice(index, 1);
    }
  }

  async createNewPdfFromSelectedPages() {
    if (this.pdfDoc && this.selectedPages.length > 0) {
      const newPdfDoc = await PDFDocument.create();
      const copiedPages = await newPdfDoc.copyPages(this.pdfDoc, this.selectedPages);

      copiedPages.forEach((page) => {
        newPdfDoc.addPage(page);
      });

      const pdfBytes = await newPdfDoc.save();
      const blob = new Blob([pdfBytes], { type: 'application/pdf' });
      const url = URL.createObjectURL(blob);
      const a = document.createElement('a');
      a.href = url;
      a.download = 'selected-pages.pdf';
      a.click();
    }

    
  }



  onFileSelected(event: any) {
    this.selectedFiles = Array.from(event.target.files);
  }

  async mergePDFs() {
    if (this.selectedFiles.length === 0) {
      alert('Please select at least one PDF file.');
      return;
    }

    const mergedPdf = await PDFDocument.create();

    for (const file of this.selectedFiles) {
      const arrayBuffer = await this.readFile(file);
      const pdf = await PDFDocument.load(arrayBuffer);
      const copiedPages = await mergedPdf.copyPages(pdf, pdf.getPageIndices());
      copiedPages.forEach(page => mergedPdf.addPage(page));
    }

    const mergedPdfBytes = await mergedPdf.save();
    this.download(mergedPdfBytes, 'merged.pdf');
  }

  readFile(file: File): Promise<ArrayBuffer> {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.onload = () => resolve(fileReader.result as ArrayBuffer);
      fileReader.onerror = error => reject(error);
      fileReader.readAsArrayBuffer(file);
    });
  }

  download(data: BlobPart, filename: string) {
    const blob = new Blob([data], { type: 'application/pdf' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    window.URL.revokeObjectURL(url);
    document.body.removeChild(a);
  }

// 
async fileSelected(event: any) {
  this.file = event.target.files[0];
}

async addWatermark() {
  if (!this.file || !this.watermarkText) {
    alert('Please select a file and enter watermark text.');
    return;
  }

  try {
    const arrayBuffer = await this.readFile(this.file);
    const watermarkedPdfBytes = await this.watermarkService.addWatermark(arrayBuffer, this.watermarkText);
    this.downloadWatermarkPDF(watermarkedPdfBytes, 'watermarked.pdf');
  } catch (error) {
    console.error('Error adding watermark:', error);
    alert('Failed to add watermark.');
  }
}

readPDFFile(file: File): Promise<ArrayBuffer> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = (event: any) => {
      resolve(event.target.result);
    };
    reader.onerror = (error) => {
      reject(error);
    };
    reader.readAsArrayBuffer(file);
  });
}

downloadWatermarkPDF(data: ArrayBuffer, filename: string) {
  const blob = new Blob([data], { type: 'application/pdf' });
  const url = window.URL.createObjectURL(blob);
  const a = document.createElement('a');
  a.href = url;
  a.download = filename;
  document.body.appendChild(a);
  a.click();
  window.URL.revokeObjectURL(url);
  document.body.removeChild(a);
}

async addBookmarks() {
  if (!this.file || this.bookmarks.length === 0) {
    alert('Please select a file and add at least one bookmark.');
    return;
  }

  try {
    const arrayBuffer = await this.readFile(this.file);
    const bookmarkedPdfBytes = await this.watermarkService.addBookmarks(arrayBuffer, this.bookmarks);
    this.download(bookmarkedPdfBytes, 'bookmarked.pdf');
  } catch (error) {
    console.error('Error adding bookmarks:', error);
    alert('Failed to add bookmarks.');
  }
}

addBookmark() {
  this.bookmarks.push({ title: this.bookmarkTitle, pageNumber: this.bookmarkPageNumber });
  this.bookmarkTitle = '';
  this.bookmarkPageNumber = 1;
}



  

}
